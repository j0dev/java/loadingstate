package nl.j0dev.loadingstate;

/**
 * Simple loading state implementation
 */
public class SimpleLoadingState implements LoadingState {
    private float progress;
    private String text;

    private LoadingNestedState parent;
    @Override
    public LoadingNestedState getParent() {
        return parent;
    }
    @Override
    public void setParent(LoadingNestedState parent) {
        this.parent = parent;
    }

    /**
     * Initialize an empty loading state
     */
    public SimpleLoadingState() {
    }

    /**
     * Initialize a loading state
     * @param text Loading text
     */
    public SimpleLoadingState(String text) {
        this.text = text;
    }

    @Override
    public float getProgress() {
        return progress;
    }

    @Override
    public void setProgress(float progress) {
        this.progress = progress;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }
}
