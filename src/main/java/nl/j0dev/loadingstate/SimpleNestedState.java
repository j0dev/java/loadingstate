package nl.j0dev.loadingstate;

import java.util.ArrayList;
import java.util.List;

/**
 * Nested loading state implementation
 */
public class SimpleNestedState implements LoadingNestedState {
    List<LoadingSubState> subStates = new ArrayList<>();
    LoadingSubState activeState;

    TextMode textMode = TextMode.PREFIX;
    String text = "";
    String divider = " - ";

    private LoadingNestedState parent;
    @Override
    public LoadingNestedState getParent() {
        return parent;
    }
    @Override
    public void setParent(LoadingNestedState parent) {
        this.parent = parent;
    }

    public SimpleNestedState() {
    }
    public SimpleNestedState(String text) {
        this.text = text;
    }

    @Override
    public void addState(LoadingSubState state) {
        this.subStates.add(state);
        state.setParent(this);
        if (this.activeState == null) {
            this.activeState = state;
        }
    }

    @Override
    public LoadingSubState addState(LoadingState state) {
        LoadingSubState lss = new WrappedSubState(state);
        this.addState(lss);
        return lss;
    }

    @Override
    public LoadingNestedSubState addState(LoadingNestedState state) {
        LoadingNestedSubState lnss = new WrappedNestedSubState(state);
        this.addState((LoadingSubState) lnss);
        return lnss;
    }

    @Override
    public LoadingSubState getCurrentState() {
        return this.activeState;
    }

    @Override
    public int getStateIndex() {
        return this.subStates.indexOf(this.activeState);
    }

    @Override
    public void setNextState() {
        this.activeState = this.subStates.get(this.subStates.indexOf(this.activeState) + 1);
    }

    @Override
    public void setState(LoadingSubState state) {
        if (this.subStates.contains(state)) {
            this.activeState = state;
        }
    }

    @Override
    public void setState(int index) {
        this.activeState = this.subStates.get(index);
    }

    @Override
    public List<LoadingSubState> getStates() {
        return List.copyOf(this.subStates);
    }

    @Override
    public float getProgress() {
        if (this.activeState == null) {
            return 0;
        }

        float progress = 0f;
        for (LoadingSubState s : this.subStates) {
            if (s.getMappedProgress() == -1) {
                this.calculateMappedProgress();
            }

            if (s != this.activeState) {
                progress += s.getMappedProgress();
            }
            if (s == this.activeState) {
                progress += (s.getProgress() * s.getMappedProgress());
                break;
            }
        }
        return progress;
    }

    /**
     * Calculates the mapped progress for dynamically mapped sub-states
     */
    private void calculateMappedProgress() {
        // TODO: Question: should there be an internal variable for this to allow re-calculation?, then again, there should be a way of invalidating them (automagically)
        List<LoadingSubState> dynamicMapped = new ArrayList<>(); // list of dynamically mapped states
        float progressLeft = 1f; // amount of progress left to divide

        for (LoadingSubState ss : this.subStates) {
            if (ss.getMappedProgress() == -1f) {
                dynamicMapped.add(ss);
            } else {
                progressLeft -= ss.getMappedProgress();
            }
        }

        for (LoadingSubState ss : dynamicMapped) {
            ss.setMappedProgress(progressLeft / dynamicMapped.size());
        }
    }

    @Override
    public void setProgress(float progress) {
        this.activeState.setProgress(progress);
    }

    /**
     * Set how the text gets put together from its sub-states
     * @param textMode text composition mode
     */
    public void setTextMode(TextMode textMode) {
        this.textMode = textMode;
    }

    /**
     * Retrieve the current set text composition mode
     * @return text composition mode
     */
    public TextMode getTextMode() {
        return this.textMode;
    }

    @Override
    public String getText() {
        if (this.activeState == null) return this.text;
        switch (this.textMode) {
            case PREFIX:
                return this.text + this.divider + this.activeState.getText();
            case SUFFIX:
                return this.activeState.getText() + this.divider + this.text;
            case OVERRIDE:
                return this.text;
            case SUBSTATE:
                return this.activeState.getText();
            default:
                return "Loading...";
        }
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Retrieve the divider used when composing the text
     * @return text divider
     */
    public String getTextDivider() {
        return this.divider;
    }

    /**
     * Set the divider used when composing the text
     * @param divider text divider
     */
    public void setTextDivider(String divider) {
        this.divider = divider;
    }

    /**
     * Text composition modes
     */
    public enum TextMode {
        /**
         * Prefix the active state's text with the nested text
         */
        PREFIX,
        /**
         * Suffix the active sub-state's text with the nested text
         */
        SUFFIX,
        /**
         * Only return the nested text
         */
        OVERRIDE,
        /**
         * Only return the active sub-state's text
         */
        SUBSTATE,
    }
}
