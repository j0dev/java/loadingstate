package nl.j0dev.loadingstate;

import java.util.List;

/**
 * Nested loading state interface
 * Allows for nesting states into an overall progress state
 */
public interface LoadingNestedState extends LoadingState {
    /**
     * Add a sub-state
     * @param state Sub-State
     */
    void addState(LoadingSubState state);

    /**
     * Add a sub-state
     * Will automatically wrap the given state into a mappable sub-state
     * @param state regular state
     * @return sub-state wrapper class
     */
    LoadingSubState addState(LoadingState state);

    /**
     * Add a nested sub-state
     * Will automatically wrap the given state into a mappable sub-state
     * @param state nested state
     * @return nested sub-state wrapper class
     */
    LoadingNestedSubState addState(LoadingNestedState state);

    /**
     * Retrieve the current active state
     * @return current active state
     */
    LoadingSubState getCurrentState();

    /**
     * Retrieve the index of the current active state
     * @return active state index
     */
    int getStateIndex();

    /**
     * Switch to the next state in the list
     */
    void setNextState();

    /**
     * Set the next state
     * Must already be present in the state list
     * @param state new active state
     */
    void setState(LoadingSubState state);

    /**
     * Set the next state
     * @param index state index
     */
    void setState(int index);

    /**
     * Retrieve all the registered states
     * @return ordered list of sub-states
     */
    List<LoadingSubState> getStates();
}
