package nl.j0dev.loadingstate;

/**
 * Nested Sub-State interface
 * This is an extension to the loading state adding progress mapping and capability for used with nested sub-states
 */
public interface LoadingNestedSubState extends LoadingSubState, LoadingNestedState {
}
