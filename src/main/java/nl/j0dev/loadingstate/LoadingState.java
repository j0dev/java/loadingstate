package nl.j0dev.loadingstate;

/**
 * Basic loading state interface
 */
public interface LoadingState {
    /**
     * Retrieve the parent nested loading state if any
     * @return parent nested loading state
     */
    LoadingNestedState getParent();

    /**
     * Set the parent nested loading state or clear it
     * @param parent Parent loading state
     */
    void setParent(LoadingNestedState parent);

    /**
     * Retrieve the current progress
     * @return progress
     */
    float getProgress();

    /**
     * Set the current progress
     * @param progress new progress
     */
    void setProgress(float progress);

    /**
     * Retrieve the loading text
     * @return loading text
     */
    String getText();

    /**
     * Set the loading text
     * @param text loading text
     */
    void setText(String text);
}
