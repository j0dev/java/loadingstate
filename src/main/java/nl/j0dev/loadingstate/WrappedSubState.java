package nl.j0dev.loadingstate;

/**
 * Wrapped sub-state wraps an already existing LoadingState and adds sub-state logic
 */
public class WrappedSubState implements LoadingSubState {
    private final LoadingState loadingState;
    private float mappedProgress = -1f;

    /**
     * Wrap a regular loading state to be used as a sub-state
     * @param loadingState Wrapped loading state
     */
    public WrappedSubState(LoadingState loadingState) {
        this.loadingState = loadingState;
    }
    /**
     * Wrap a regular loading state to be used as a sub-state
     * @param loadingState Wrapped loading state
     * @param mappedProgress mapped progress amount
     */
    public WrappedSubState(LoadingState loadingState, float mappedProgress) {
        this.loadingState = loadingState;
        this.mappedProgress = mappedProgress;
    }

    @Override
    public float getMappedProgress() {
        return this.mappedProgress;
    }

    @Override
    public void setMappedProgress(float mappedProgress) {
        this.mappedProgress = mappedProgress;
    }


    @Override
    public float getProgress() {
        return this.loadingState.getProgress();
    }

    @Override
    public void setProgress(float progress) {
        this.loadingState.setProgress(progress);
    }

    @Override
    public String getText() {
        return this.loadingState.getText();
    }

    @Override
    public void setText(String text) {
        this.loadingState.setText(text);
    }

    @Override
    public LoadingNestedState getParent() {
        return this.loadingState.getParent();
    }

    @Override
    public void setParent(LoadingNestedState parent) {
        this.loadingState.setParent(parent);
    }
}
