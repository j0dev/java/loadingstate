package nl.j0dev.loadingstate;

public class SimpleNestedSubState extends SimpleNestedState implements LoadingNestedSubState {
    private float mappedProgress = -1f;

    public SimpleNestedSubState() {
        super();
    }
    public SimpleNestedSubState(String text) {
        super(text);
    }
    public SimpleNestedSubState(String text, float mappedProgress) {
        super(text);
        this.mappedProgress = mappedProgress;
    }

    @Override
    public float getMappedProgress() {
        return this.mappedProgress;
    }

    @Override
    public void setMappedProgress(float mappedProgress) {
        this.mappedProgress = mappedProgress;
    }
}
