package nl.j0dev.loadingstate;

/**
 * Sub-State interface
 * This is an extension to the loading state adding progress mapping capability for use with nested states
 */
public interface LoadingSubState extends LoadingState {
    /**
     * Retrieve the amount of progress the state should use in the parent state
     * @return mapped progress amount
     */
    float getMappedProgress();

    /**
     * Set the amount of progress the state should use in the parent state
     * @param mappedProgress mapped progress amount
     */
    void setMappedProgress(float mappedProgress);
}
