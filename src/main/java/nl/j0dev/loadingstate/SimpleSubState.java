package nl.j0dev.loadingstate;

/**
 * Simple sub state implementation
 */
public class SimpleSubState extends SimpleLoadingState implements LoadingSubState {
    private float mappedProgress = -1f;

    public SimpleSubState() {
        super();
    }
    public SimpleSubState(String text) {
        super(text);
    }
    public SimpleSubState(String text, float mappedProgress) {
        super(text);
        this.mappedProgress = mappedProgress;
    }

    @Override
    public float getMappedProgress() {
        return this.mappedProgress;
    }

    @Override
    public void setMappedProgress(float mappedProgress) {
        this.mappedProgress = mappedProgress;
    }
}
