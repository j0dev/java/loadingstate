package nl.j0dev.loadingstate;

import java.util.List;

/**
 * Wrapped nested sub-state wraps an already existing NestedLoadingState and adds sub-state logic
 */
public class WrappedNestedSubState implements LoadingNestedSubState {
    private final LoadingNestedState loadingNestedState;
    private float mappedProgress = -1f;

    public WrappedNestedSubState(LoadingNestedState loadingNestedState) {
        this.loadingNestedState = loadingNestedState;
    }

    public WrappedNestedSubState(LoadingNestedState loadingNestedState, float mappedProgress) {
        this.loadingNestedState = loadingNestedState;
        this.mappedProgress = mappedProgress;
    }

    @Override
    public float getMappedProgress() {
        return this.mappedProgress;
    }

    @Override
    public void setMappedProgress(float mappedProgress) {
        this.mappedProgress = mappedProgress;
    }


    @Override
    public float getProgress() {
        return this.loadingNestedState.getProgress();
    }

    @Override
    public void setProgress(float progress) {
        this.loadingNestedState.setProgress(progress);
    }

    @Override
    public String getText() {
        return this.loadingNestedState.getText();
    }

    @Override
    public void setText(String text) {
        this.loadingNestedState.setText(text);
    }

    @Override
    public LoadingNestedState getParent() {
        return this.loadingNestedState.getParent();
    }

    @Override
    public void setParent(LoadingNestedState parent) {
        this.loadingNestedState.setParent(parent);
    }


    @Override
    public void addState(LoadingSubState state) {
        this.loadingNestedState.addState(state);
    }

    @Override
    public LoadingSubState addState(LoadingState state) {
        return this.loadingNestedState.addState(state);
    }

    public LoadingNestedSubState addState(LoadingNestedState state) {
        return this.loadingNestedState.addState(state);
    }


    @Override
    public LoadingSubState getCurrentState() {
        return loadingNestedState.getCurrentState();
    }

    @Override
    public int getStateIndex() {
        return this.loadingNestedState.getStateIndex();
    }

    @Override
    public void setNextState() {
        this.loadingNestedState.setNextState();
    }

    @Override
    public void setState(LoadingSubState state) {
        this.loadingNestedState.setState(state);
    }

    @Override
    public void setState(int index) {
        this.loadingNestedState.setState(index);
    }

    @Override
    public List<LoadingSubState> getStates() {
        return this.loadingNestedState.getStates();
    }
}
